const int buttonPin = 2;
const int ledPin = 1;
int ledState = LOW;
const int relayPin =  0;
int buttonState = 0;  //variable for reading the pushbutton status
int buttonFlag = 0;  //keep track of the button
long i = 0; //counting variable
unsigned long previousMillis = 0;
const long interval = 1000;

void setup() {
  pinMode(relayPin, OUTPUT);
  pinMode(ledPin, OUTPUT);      
  pinMode(buttonPin, INPUT); //wired to a momentary pushbutton
}

void loop() {
  //read the state of the pushbutton value:
  buttonState = digitalRead(buttonPin);
  delay(50);
  //check if the pushbutton is pressed, the device is off, and voltage is ok
  //if they are, turn everything on and set the flag
  if (buttonState == HIGH && buttonFlag == 0 && battery_voltage() == 1) {
    turn_on();
  } 
  //check if the pushbutton is pressed and if the device is on
  //if they are, turn everything off and set the flag
  else if (buttonState == HIGH && buttonFlag == 1) {
    turn_off();
  }
  
  //if the battery voltage drops, delay for 30 minutes before turning back on
  //this lets the solar panel charge the battery up some before turning on the inverter
  if (battery_voltage() == 0 && buttonFlag == 1) {
 //   delay(400);  //wait a bit and check again, prevents trips due to inrush current
 //   if (battery_voltage() == 0) {  //if the battery voltage is still low:
      digitalWrite(relayPin, LOW);  //do not use turn_off() because that would set the button flag
      while(i < 900) {  //i = seconds, 15min*60sec=900 sec
        //ADD BUTTON DETECTION STUFF HERE NEXT
        buttonState = digitalRead(buttonPin);
        delay(50);
        if (buttonState == HIGH) {
          turn_off();
          //escape the while loop?
          break;
        }
        //blink without delay stuff:
        unsigned long currentMillis = millis();
        if(currentMillis - previousMillis >= interval) { //this stuff happens every interval:
          previousMillis = currentMillis;   
          if (ledState == LOW)
            ledState = HIGH;
          else
            ledState = LOW;
          // set the LED with the ledState of the variable:
          digitalWrite(ledPin, ledState);
          i++;
        }
      }
      
      i = 0; //reset the counter for the next time
//    }
     //moved else if from here. something was keeping it from coming back on
  }
  //if the button had been pushed, turn everything back on if the voltage recovers
  else if (battery_voltage() == 1 && buttonFlag == 1) { //ADDED ELSE IF, MAYBE REMOVE ELSE??
    turn_on();
  }
}

//reads the battery voltage and returns 0 if it falls below 11.4V
int battery_voltage(void) {
  long sensorValue = analogRead(A2);
  if (sensorValue < 535) {
    return 0;
  }
  else {
    return 1;
  }
}

void turn_on(void) {
  ledState = HIGH;
  digitalWrite(ledPin, ledState); 
  digitalWrite(relayPin, HIGH);
  buttonFlag = 1;
  buttonState = 0;
  delay(1000);  //debounce delay 
}

void turn_off(void) {
  ledState = LOW;
  digitalWrite(ledPin, ledState); 
  digitalWrite(relayPin, LOW);
  buttonFlag = 0;
  buttonState = 0;
  delay(1000); 
}